EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title "Vy-pi-nac"
Date "2021-09-30"
Rev "1.0.0"
Comp "(c) qyx@krtko.org, CC-BY-SA 4.0"
Comment1 "Wall switch controller with CAN-FD interface"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 1000 1000 0    100  ~ 20
Vy-pi-nac
Text Notes 1000 1200 0    50   ~ 10
Wall switch controller with CAN-FD interface, 4 inputs, LED outputs and 2 NTC sensor inputs
$Sheet
S 1000 2000 1800 1000
U 6156E788
F0 "Interfaces" 50
F1 "interfaces.sch" 50
$EndSheet
$Sheet
S 3000 2000 1800 1000
U 6156E809
F0 "Power management" 50
F1 "power.sch" 50
$EndSheet
$Sheet
S 5000 2000 1800 1000
U 6156E861
F0 "MCU & companion" 50
F1 "mcu.sch" 50
$EndSheet
$Sheet
S 7000 2000 1800 1000
U 6156E8B1
F0 "Bus connection" 50
F1 "bus.sch" 50
$EndSheet
$Comp
L symbols:symbol_placeholder SYM1
U 1 1 6156EE97
P 1300 7000
F 0 "SYM1" H 1300 6600 50  0000 C CNN
F 1 "CE logo" H 1300 6500 50  0000 C CNN
F 2 "Symbol:CE-Logo_8.5x6mm_SilkScreen" H 1300 7000 50  0001 C CNN
F 3 "" H 1300 7000 50  0001 C CNN
	1    1300 7000
	1    0    0    -1  
$EndComp
$Comp
L symbols:symbol_placeholder SYM2
U 1 1 6156FAE8
P 2100 7000
F 0 "SYM2" H 2100 6600 50  0000 C CNN
F 1 "WEEE logo" H 2100 6500 50  0000 C CNN
F 2 "Symbol:WEEE-Logo_4.2x6mm_SilkScreen" H 2100 7000 50  0001 C CNN
F 3 "" H 2100 7000 50  0001 C CNN
	1    2100 7000
	1    0    0    -1  
$EndComp
$Comp
L symbols:symbol_placeholder SYM3
U 1 1 6157003A
P 2900 7000
F 0 "SYM3" H 2900 6600 50  0000 C CNN
F 1 "OSHW logo" H 2900 6500 50  0000 C CNN
F 2 "Symbol:OSHW-Symbol_6.7x6mm_SilkScreen" H 2900 7000 50  0001 C CNN
F 3 "" H 2900 7000 50  0001 C CNN
	1    2900 7000
	1    0    0    -1  
$EndComp
$EndSCHEMATC
