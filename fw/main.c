#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/systick.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/fdcan.h>
#include <libopencm3/stm32/adc.h>
#include <libopencm3/stm32/pwr.h>
#include <libopencm3/stm32/spi.h>


/********************** ID & command offsets **********************/
const uint32_t can_id = 0x80;
#define GET_STATUS_OFFSET 0
#define GET_TEMP_OFFSET 1
#define SET_LED_OFFSET 2
#define SET_WS281X_OFFSET 3


/********************** GPIO definitions **********************/
#define CAN_SHDN_PORT GPIOA
#define CAN_SHDN_PIN GPIO10
#define CAN_RX_PORT GPIOA
#define CAN_RX_PIN GPIO11
#define CAN_TX_PORT GPIOA
#define CAN_TX_PIN GPIO12
#define CAN_AF GPIO_AF9

#define NTC1_CHANNEL 1
#define NTC1_PORT GPIOA
#define NTC1_PIN GPIO0

#define NTC2_CHANNEL 2
#define NTC2_PORT GPIOA
#define NTC2_PIN GPIO1

#define DIN_PORT GPIOB
#define DIN1_PIN GPIO3
#define DIN2_PIN GPIO4
#define DIN3_PIN GPIO5
#define DIN4_PIN GPIO6

#define LED_OE_PORT GPIOA
#define LED_OE_PIN GPIO5

#define LED1_PORT GPIOA
#define LED1_PIN GPIO7
#define LED1_WS_AF GPIO_AF5


static void delay(uint32_t d) {
	for (uint32_t i = 0; i < d; i++) {
		__asm__("NOP");
	}
}


static void clock_setup(void) {
	rcc_osc_on(RCC_HSE);
	rcc_wait_for_osc_ready(RCC_HSE);
	rcc_set_sysclk_source(RCC_HSE);

	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_GPIOB);

	/* Use SYSCLK as an independent ADC clock source. */
	RCC_CCIPR |= RCC_CCIPR_ADC12_SYS << RCC_CCIPR_ADC12_SHIFT;
	RCC_CCIPR |= RCC_CCIPR_ADC345_SYS << RCC_CCIPR_ADC345_SHIFT;
	rcc_periph_clock_enable(RCC_ADC1);

	/* Enable FDCAN clock, keep the default core clock (HSE). */
	rcc_periph_clock_enable(RCC_FDCAN);
	rcc_periph_clock_enable(SCC_FDCAN);

	/* Disable USB CC 5k1 pull-downs. They are messing with PB4 and PB6 inputs. */
	rcc_periph_clock_enable(RCC_PWR);
	PWR_CR3 |= PWR_CR3_UCPD1_DBDIS;

	/* SPI for driving WS281x LED strips. */
	rcc_periph_clock_enable(RCC_SPI1);

	/* Run a systick at 100 Hz. */
	nvic_set_priority(NVIC_SYSTICK_IRQ, 255);
	systick_set_clocksource(STK_CSR_CLKSOURCE_AHB);
	systick_set_reload(159999);
	systick_interrupt_enable();
}


static void gpio_setup(void) {
	/* Status LEDs */
	gpio_mode_setup(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO3);
	gpio_mode_setup(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO2);

	gpio_mode_setup(NTC1_PORT, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, NTC1_PIN);
	gpio_mode_setup(NTC2_PORT, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, NTC2_PIN);

	/* Digital inputs. There are expernal 10k pull-ups. */
	gpio_mode_setup(DIN_PORT, GPIO_MODE_INPUT, GPIO_PUPD_NONE, DIN1_PIN | DIN2_PIN | DIN3_PIN | DIN4_PIN);

	/* CAN port. Enable the transceiver permanently (SHDN = L). */
	gpio_mode_setup(CAN_SHDN_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, CAN_SHDN_PIN);
	gpio_clear(CAN_SHDN_PORT, CAN_SHDN_PIN);
	gpio_mode_setup(CAN_RX_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, CAN_RX_PIN);
	gpio_set_af(CAN_RX_PORT, CAN_AF, CAN_RX_PIN);
	gpio_mode_setup(CAN_TX_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, CAN_TX_PIN);
	gpio_set_af(CAN_TX_PORT, CAN_AF, CAN_TX_PIN);

	/* LED level shifter output enable */
	gpio_mode_setup(LED_OE_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, LED_OE_PIN);
	gpio_clear(LED_OE_PORT, LED_OE_PIN);

	/* LED1 output AF for SPI1. Pin mode is set before data transmission. */
	gpio_set_af(LED1_PORT, LED1_WS_AF, LED1_PIN);
}


static void can_setup(void) {
	/* Enable CAN with a 4 MHz clock. One bit is 16 TQ. The resulting bitrate is 250k. */
	rcc_periph_reset_pulse(RST_FDCAN);
	fdcan_init(CAN1, FDCAN_CCCR_INIT_TIMEOUT);
	fdcan_set_can(CAN1, false, false, true, false, 1, 8, 5, 3);
	fdcan_set_fdcan(CAN1, true, true, 1, 8, 5, 3);
	fdcan_start(CAN1, FDCAN_CCCR_INIT_TIMEOUT);
}


static void adc_setup(uint32_t adc) {
	adc_disable_deeppwd(adc);
	adc_enable_regulator(adc);
	adc_enable_vrefint();
	adc_set_sample_time_on_all_channels(adc, ADC_SMPR_SMP_92DOT5CYC);
	adc_calibrate(adc);
	while (adc_is_calibrating(adc)) {
		;
	}
	adc_power_on(adc);
}


static uint32_t adc_read_raw(uint32_t adc, uint8_t channel) {
	uint8_t channels[16];

	channels[0] = channel;
	adc_set_regular_sequence(adc, 1, channels);
	adc_start_conversion_regular(adc);
	while (!adc_eoc(adc)) {
		;
	}

	return adc_read_regular(adc);
}


/* TODO: handle hardware oversampling */
static uint32_t adc_read_raw_oversampled(uint32_t adc, uint8_t channel, uint32_t c) {
	uint32_t r = 0;
	for (uint32_t i = 0; i < c; i++) {
		r += adc_read_raw(adc, channel);
	}
	return r;
}


static void get_temperature(void) {
	/* Oversample the input a bit. */
	uint16_t ntc1_raw = adc_read_raw_oversampled(ADC1, NTC1_CHANNEL, 64) >> 3;
	uint16_t ntc2_raw = adc_read_raw_oversampled(ADC1, NTC2_CHANNEL, 64) >> 3;

	const uint8_t status_data[] = {
		ntc1_raw >> 8,
		ntc1_raw & 0xff,
		ntc2_raw >> 8,
		ntc2_raw & 0xff
	};

	/* Do not handle the return value. Do what we can. Send the measured temperature. */
	fdcan_transmit(CAN1, can_id + GET_TEMP_OFFSET, false, false, false, false, sizeof(status_data), status_data);
}


/* Keep the number of ticks since the button was pressed down. */
uint32_t din_counters[4] = {0};
static uint8_t process_din(uint8_t n, bool din) {
	uint8_t status = 0;

	if (din && din_counters[n] == 0) {
		/* Push down event. Start counting. */
		status |= 0x01;
		din_counters[n]++;
	} else if (din && din_counters[n] >= 10) {
		/* 100 ms passed from the push or the last update event.
		 * Generate a new update event. */
		status |= 0x02;

		/* Restart counting. */
		din_counters[n] = 1;
	} else if (din && din_counters[n] < 10) {
		/* Button is still pushed. Just count ticks. */
		din_counters[n]++;
	} else if (!din && din_counters[n] > 0) {
		/* Release event. Stop counting. */
		status |= 0x04;
		din_counters[n] = 0;
	}

	return status;
}


static void get_din(void) {
	/* Sample all inputs and construct a status message. */
	const uint8_t status_data[4] = {
		process_din(0, gpio_get(DIN_PORT, DIN1_PIN) == 0),
		process_din(1, gpio_get(DIN_PORT, DIN2_PIN) == 0),
		process_din(2, gpio_get(DIN_PORT, DIN3_PIN) == 0),
		process_din(3, gpio_get(DIN_PORT, DIN4_PIN) == 0)
	};

	/* Send the message only if at least one event happened. */
	if (status_data[0] | status_data[1] | status_data[2] | status_data[3]) {
		fdcan_transmit(CAN1, can_id + GET_STATUS_OFFSET, false, false, false, false, sizeof(status_data), status_data);
	}
}


static void ws_setup(void) {
	/* Set SPI baudrate to 4 MHz, transmit mode only over a MOSI line.
	 * SCK is not used nor configured. */
	spi_set_master_mode(SPI1);
	spi_set_baudrate_prescaler(SPI1, SPI_CR1_BR_FPCLK_DIV_4);
	spi_set_clock_polarity_0(SPI1);
	spi_set_clock_phase_0(SPI1);
	spi_send_msb_first(SPI1);
	spi_set_bidirectional_transmit_only_mode(SPI1);
	spi_enable_software_slave_management(SPI1);
	spi_set_nss_high(SPI1);

	/* We are using 8 SPI bits to send a single byte in the WS281x stream. */
	spi_fifo_reception_threshold_8bit(SPI1);
	spi_set_data_size(SPI1, SPI_CR2_DS_8BIT);
	spi_enable(SPI1);
}


static void ws_start(void) {
	/* Fill buffer with zero before reconfiguring the GPIO. This avoids the glitch on MOSI. */
	spi_send8(SPI1, 0x00);
	gpio_set_output_options(LED1_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, LED1_PIN);
	gpio_mode_setup(LED1_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, LED1_PIN);
	/* Make a reset pulse. */
	for (uint32_t i = 0; i < 100; i++) {
		spi_send8(SPI1, 0x00);
	};
}


static void ws_bit(uint8_t b) {
	if (b) {
		/* Single SPI clock is 0.25 us. Send 1 as 1.5 us high pulse, 0.5 us low pulse. */
		spi_send8(SPI1, 0xfc);
	} else {
		/* Send 0 as 0.25 us high, 1.75 us low pulse. */
		spi_send8(SPI1, 0x80);
	}
}


static void ws_byte(uint8_t b) {
	/* Send one WS281x byte/value (R, G or B). Too lazy for a loop. */
	ws_bit(b & 0x80);
	ws_bit(b & 0x40);
	ws_bit(b & 0x20);
	ws_bit(b & 0x10);
	ws_bit(b & 0x08);
	ws_bit(b & 0x04);
	ws_bit(b & 0x02);
	ws_bit(b & 0x01);
}


static void ws_end(void) {
	/* Fill the FIFO with zeroes and unconfigure GPIO. */
	spi_send8(SPI1, 0x00);
	spi_send8(SPI1, 0x00);
	spi_send8(SPI1, 0x00);
	spi_send8(SPI1, 0x00);
	gpio_clear(LED1_PORT, LED1_PIN);
	gpio_mode_setup(LED1_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, LED1_PIN);
}


/* We are keeping individual color values of the whole strip in an array. */
static struct {
	uint8_t r, g, b;
} ws_data[256];

/* Maximum number of LEDs in the strip to update. It is increased when a set
 * request is received with a higher number .*/
static uint8_t ws_count = 0;

static void ws_update(void) {
	ws_start();
	for (uint32_t i = 0; i < ws_count; i++) {
		ws_byte(ws_data[i].g);
		ws_byte(ws_data[i].r);
		ws_byte(ws_data[i].b);
	}
	ws_end();
}


static void ws_set(uint8_t n, uint8_t r, uint8_t g, uint8_t b) {
	if ((n + 1) > ws_count) {
		ws_count = n + 1;
	}
	ws_data[n].r = r;
	ws_data[n].g = g;
	ws_data[n].b = b;
}


/* Clear the maximum possible strip length without modyfiying the color data. */
static void ws_clear(void) {
	ws_start();
	for (uint32_t i = 0; i < 768; i++) {
		ws_byte(0x00);
	}
	ws_end();
}


uint32_t tick_counter = 0;
void sys_tick_handler(void) {
	tick_counter++;

	/* Report NTC temperature every second. */
	if ((tick_counter % 100) == 50) {
		get_temperature();
	}

	/* Toggle the status LED, but not too fast. */
	if ((tick_counter % 20) == 0) {
		gpio_toggle(GPIOA, GPIO3);
	}

	/* Update WS281x every 100 ms */
	if ((tick_counter % 10) == 0) {
		ws_update();
	}

	/* Determine button status every tick. */
	get_din();
}


static void can_rx(void) {
	uint8_t rx_data[8] = {0};
	uint32_t rx_id;
	bool rx_ext_id;
	bool rx_rtr;
	uint8_t rx_fmi;
	uint8_t rx_length;
	uint16_t rx_timestamp;

	int status = fdcan_receive(CAN1, FDCAN_FIFO0, true, &rx_id, &rx_ext_id, &rx_rtr, &rx_fmi, &rx_length, rx_data, &rx_timestamp);

	if (status == FDCAN_E_OK) {
		gpio_set(GPIOA, GPIO2);
		if (rx_id == (can_id + SET_WS281X_OFFSET) && rx_length == 4) {
			ws_set(rx_data[0], rx_data[1], rx_data[2], rx_data[3]);
		}
		gpio_clear(GPIOA, GPIO2);
	}
}


int main(void) {
	clock_setup();
	gpio_setup();
	can_setup();
	adc_setup(ADC1);
	ws_setup();
	systick_counter_enable();

	ws_clear();
	while (1) {
		/* There is nothing else to do, we can continuously check for new CAN messages. */
		can_rx();
	}

	return 0;
}
