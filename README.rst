A wall-switch retrofit module with CAN interface, temperature sensor and a LED driver
===========================================================================================

Main features
-------------------

- JST 2 mm pitch connectors
- two inputs for 10K NTC temperature sensors
- 4 digital inputs for connecting the old wall switch
- 4 5V PWM outputs for LEDs, one of them is capable of driving a WS281x LED strip
- CAN interface
- 24 V power supply (6-30 V range)
- proper input/output protections
- low current consumption in idle


BOM for pcb-1.0.0
---------------------------

3x ESDA6V1-5SC6 0.287€/100
4x RN 4x0603 0.03€/1000
2x 6-pin connector
1x 4 pin connector
1x 74AHCT125BQ 0.33€/10
3x TBU-CA065-200 1.02€/100
1x RV 40 V MLVC18V030C1200 0.756€/1%
1x L 3u3/3030 0.5 A DJNR3015-3R3 0.105€/10
1x L 1u5/3030 1 A DJNR3015-2R (2u2 alt) 0.105€/10
1x SMBJ24A 0.08€/25
1x TPS70633DRV 0.678€/10
1x LMR33630APAQRNXRQ1 2.08€/25
4x C 22u/10V CL21A226MPCLRNC 0805 0.08€/100
1x XTAL 32768 3215 0.6€/10
1x STM32G0B1KxTx (STM32G0B1KET6) 3.93€/25
2x LED 0805
1x MCP2562-E/MF / alt. TLT9251 1.38€/25
1x SRF3216-261Y 0.6€/100
1x SL1010A090SM 1.16€/10
1x power connector
1x bus connector

total 16.165€
